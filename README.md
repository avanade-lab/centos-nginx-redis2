# *centos-nginx-redis2*

Projeto de criação de um container com CentOS 7 com Nginx + Redis + Modulo-Nginx-Redis2 + Nginx-Module-Set-Misc + Nginx-Module-Devel-kit

## Resumo ##

Este projeto cria um container rodando CentOS 7 com Nginx e Redis + Modulos que tem como objetivo fazer o nginx virar um poderoso cache de conteúdo armazenado no Redis.

## Download ##

Baixe o projeto em sua maquina e siga os passos de execução abaixo

## Requisitos ##

Necessário o docker instalado

## Executar ##

Linux

> $ ./build.sh


Windows

> .\build.bat

## Testar ##

Após o container executado e carregado, abra uma ferramenta para fazer requisições de API ex. Postman e execute os seguintes testes:

**Healh Check**

![Alt text](docs/img1.jpg "Image 1")

GET - http://localhost:8080/health \
RETURN - +OK 

**Get value**

![Alt text](docs/img3.jpg "Image 3")

GET - http://localhost:8080/get?key=parametro \
Query params - key=nome_da_chave \
Return - retorno_valor_chave 

**Post value**

![Alt text](docs/img2.jpg "Image 2")

POST - http://localhost:8080/post?key=parametro&val=valor \
Query params - key=nome_da_chave \
Query params - val=valor_da_chave \
Return - +OK

**Delete value**

![Alt text](docs/img4.jpg "Image 4")

DEL - http://localhost:8080/del?key=parametro \
Query params - key=nome_da_chave \
Return - :1 


## Informações ##

Baixe o conteúdo e siga os passos de inicialização conforme o seu sistema operacional e observe os logs de saída.

## Arquivos ##


| Arquivo | Info | Comentários |
| --- | --- | --- |
| build.bat | Arquivo Batch Windows | Inicialização para Windows  |  
| build.sh | Arquivo Shellscript Linux | Inicialização para Linux |  
| Dockerfile | Arquivo Dockerfile config | Configuração de construição Docker |  
| README.md | Arquivo README  | Leiame |  
| scripts/config.sh | Arquivo Shellscript config | Configuração do Linux pós inicialização do container |  
| scripts/nginx.conf | Arquivo Nginx config | Configuração do Nginx |  
| docs/NGINX.postman_collection.json | Arquivo JSON Postman | Arquivo para importar no Postman para testes | 
| docs/img1.jpg | Arquivo JPG | Imagem de exemplo 1 Postman |  
| docs/img2.jpg | Arquivo JPG | Imagem de exemplo 2 Postman |  
| docs/img3.jpg | Arquivo JPG | Imagem de exemplo 3 Postman |  
| docs/img4.jpg | Arquivo JPG | Imagem de exemplo 4 Postman |  

## Sobre o Dockerfile ##

> FROM centos:7
> 
> MAINTAINER "Administrator" <admin@localhost>
> ENV container docker
> RUN yum -y update; yum clean all
> RUN yum -y install systemd; yum clean all; \
> (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == > systemd-tmpfiles-setup.service ] || rm -f $i; done); \
> rm -f /lib/systemd/system/multi-user.target.wants/*;\
> rm -f /etc/systemd/system/*.wants/*;\
> rm -f /lib/systemd/system/local-fs.target.wants/*; \
> rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
> rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
> rm -f /lib/systemd/system/basic.target.wants/*;\
> rm -f /lib/systemd/system/anaconda.target.wants/*;
> VOLUME [ "/sys/fs/cgroup" ]
> CMD ["/usr/sbin/init"]
> 
> WORKDIR home/
> 
> COPY ${UP_FILE} /home

## Sobre o Config.sh ##

> yum -y update\
> yum -y install epel-release\
> yum -y install nano\
> yum -y install wget\
> yum -y install openssl-devel\
> yum -y groupinstall "Development Tools"\
> yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel\
> yum -y install net-tools\
> yum -y install redis\
> systemctl start redis.service\
> systemctl status redis.service\
> systemctl enable redis.service\
> redis-server -v\
> redis-cli -v\
> yum -y install nginx\
> systemctl start nginx\
> systemctl status nginx\
> systemctl enable nginx\
> nginx -v\
> nginx -t\
> cd /home/\
> wget http://nginx.org/download/nginx-1.16.1.tar.gz\
> tar -xzvf nginx-1.16.1.tar.gz\
> wget https://github.com/openresty/redis2-nginx-module/archive/refs/tags/v0.15.tar.gz\
> tar -xzvf v0.15.tar.gz\
> wget https://github.com/openresty/set-misc-nginx-module/archive/refs/tags/v0.32.tar.gz\
> tar -xzvf v0.32.tar.gz\
> wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz\
> tar -xzvf v0.3.1.tar.gz\
> cd /home/nginx-1.16.1\
> ./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log > --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' --add-dynamic-module=/home/ngx_devel_kit-0.3.1  --add-dynamic-module=/home/set-misc-nginx-module-0.32 --add-dynamic-module=/home/redis2-nginx-module-0.15\
> #make modules\
> make -j2\
> make install\
> ls -la /home/nginx-1.16.1/objs/\
> cp /home/nginx-1.16.1/objs/ngx_http_redis2_module.so /usr/lib64/nginx/modules/\
> cp /home/nginx-1.16.1/objs/ndk_http_module.so /usr/lib64/nginx/modules/\
> cp /home/nginx-1.16.1/objs/ngx_http_set_misc_module.so /usr/lib64/nginx/modules/\
> yes | cp -v /home/scripts/nginx.conf /etc/nginx/\
> mkdir /var/cache/nginx/\
> mkdir /var/cache/nginx/client_temp/\
> nginx -s reload\
> nginx -t\
> nginx -v


## Recursos utilizados ##

Abaixo todos os recursos utilizados. 

https://www.docker.com/ \
https://www.centos.org/ \
https://www.nginx.com/ \
https://redis.io/ \
https://github.com/openresty/redis2-nginx-module \
https://github.com/openresty/set-misc-nginx-module \
https://github.com/vision5/ngx_devel_kit
