echo =====================================================================================================================================================================
echo PROCESSO INSTALAÇÃO E ATUALIZAÇÃO CENTOS 
echo =====================================================================================================================================================================
echo Iniciando Atualização do CentOS
yum -y update
echo Instalando EPEL-RELEASE
yum -y install epel-release
echo Instalando Dependencias
yum -y install nano
yum -y install wget
echo Instalando Ferramentas
yum -y install openssl-devel
yum -y groupinstall "Development Tools"
yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel
yum -y install net-tools
echo =====================================================================================================================================================================
echo PROCESSO INSTALAÇÃO DE FERRAMENTAS 
echo =====================================================================================================================================================================
echo Instalando Redis
yum -y install redis
echo Inicializando e ativando Redis
systemctl start redis.service
systemctl status redis.service
systemctl enable redis.service
echo Verificando Redis
redis-server -v
redis-cli -v
echo Instalando Nginx
yum -y install nginx
echo Inicializando e ativando Nginx
systemctl start nginx
systemctl status nginx
systemctl enable nginx
echo Verificando Nginx
nginx -v
nginx -t
echo =====================================================================================================================================================================
#read -p "Processamento concluido, pressione [Enter] para continuar..."
echo
echo =====================================================================================================================================================================
echo PROCESSO INSTALAÇÃO DE FERRAMENTAS 
echo =====================================================================================================================================================================
echo Preparando Nginx-Module
cd /home/
echo Baixando fontes Nginx-1.16.1
wget http://nginx.org/download/nginx-1.16.1.tar.gz
echo Descompactando Nginx-1.16.11
tar -xzvf nginx-1.16.1.tar.gz
echo Baixando Nginx-Module-Redis-2
wget https://github.com/openresty/redis2-nginx-module/archive/refs/tags/v0.15.tar.gz
echo Descompactando Nginx-Module-Redis-2
tar -xzvf v0.15.tar.gz
echo Baixando Nginx-Module-Set-Misc
wget https://github.com/openresty/set-misc-nginx-module/archive/refs/tags/v0.32.tar.gz
tar -xzvf v0.32.tar.gz
echo Baixando Nginx-Module-Devel-kit
wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz
tar -xzvf v0.3.1.tar.gz
echo Preparando compilação Nginx-Module-Redis-2
cd /home/nginx-1.16.1
./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' --add-dynamic-module=/home/ngx_devel_kit-0.3.1  --add-dynamic-module=/home/set-misc-nginx-module-0.32 --add-dynamic-module=/home/redis2-nginx-module-0.15
echo Compilando Nginx-Module-Redis
#make modules
make -j2
make install
echo Detalhes Modulos gerados em /home/nginx-1.16.1/objs/
ls -la /home/nginx-1.16.1/objs/
echo =====================================================================================================================================================================
#read -p "Verifique os arquivos gerados e pressione [Enter] para continuar..."
echo Copiando ngx_http_redis2_module.so, ngx_http_set_misc_module.so, ndk_http_module.so para /usr/lib64/nginx/modules/
cp /home/nginx-1.16.1/objs/ngx_http_redis2_module.so /usr/lib64/nginx/modules/
cp /home/nginx-1.16.1/objs/ndk_http_module.so /usr/lib64/nginx/modules/
cp /home/nginx-1.16.1/objs/ngx_http_set_misc_module.so /usr/lib64/nginx/modules/
echo Atualizando arquivos de Configuração Nginx
yes | cp -v /home/scripts/nginx.conf /etc/nginx/
mkdir /var/cache/nginx/
mkdir /var/cache/nginx/client_temp/
echo Re-inicializando Nginx
nginx -s reload
nginx -t
nginx -v
echo Reinicializando Nginx
systemctl start redis.service
echo =====================================================================================================================================================================
#read -p "Processamento concluido, pressione [Enter] para continuar..."
echo
echo =====================================================================================================================================================================
echo PROCESSO CONCLUIDO
echo =====================================================================================================================================================================
